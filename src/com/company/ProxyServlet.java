package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class ProxyServlet {
    private static Logger logger = Logger.getLogger("ProxyLog");

    public static void main(String[] args) throws IOException {
        String address = "http://127.0.0.1:8081";
        int numThreads = 100;
        Executor threadPool = Executors.newFixedThreadPool(numThreads);
        int port = 80;
        String logFileName = "log\\ProxyLogFile.log";
        Collection<String> servlets = new ArrayList<String>();

        ServerSocket socket = new ServerSocket(port);
        FileHandler fh;
        fh = new FileHandler(logFileName);
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);

        servlets = GetServlets(address);

        if (servlets.size() == 0) {
            logger.warning("Can't start server. No servlets available.");
        } else {
            while (true) {
                final Socket connection = socket.accept();
                Runnable task = new Runnable() {
                    @Override
                    public void run() {
                        HandleRequest(connection);
                    }
                };
                threadPool.execute(task);
            }
        }
    }

    private static Collection<String> GetServlets(String address) {
        Collection<String> servlets = new ArrayList<String>();

        URL url = null;
        try {
            url = new URL(address);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            logger.info(content.toString());
            in.close();
        } catch (MalformedURLException e) {
            logger.severe(e.getMessage());
        } catch (ProtocolException e) {
            logger.severe(e.getMessage());
        } catch (IOException e) {
            logger.severe(e.getMessage());
        }

        return servlets;
    }

    private static void HandleRequest(Socket s) {
        BufferedReader in;
        PrintWriter out;
        String request;

        try {
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));

            request = in.readLine();
            logger.info(request);

            out = new PrintWriter(s.getOutputStream(), true);
            out.println("HTTP/1.0 200");
            out.println("Content-type: text/plain");
            out.println("Server-name: proxyserver");
            String response = "blah blah blah";
            out.println("Content-length: " + response.length());
            out.println("");
            out.println(response);
            out.flush();
            out.close();
            s.close();
        } catch (IOException e) {
            logger.severe(e.getMessage());
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (IOException e) {
                    logger.severe(e.getMessage());
                }
            }
        }
        return;
    }

}